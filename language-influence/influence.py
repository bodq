#!/usr/bin/python
"""Compute and plot influence graph between entities.

Give a name to define the influence subgraph. Example:

  ./influence.py C++  # Will create C++.png

Other interesting names:
  'Carl Jung', 'Stanley Kubrick', 'Vincent van Gogh'
"""

# Builtin imports.
import itertools
import pprint
import random

import dbpedia_parse
import dot
import graphs
import sys

def log(format, *args):
  print >>sys.stderr, format % args

def TidyEntityName(s):
  return s.replace('programming_language', '').replace('()', '').replace('_', ' '
      ).strip()

def TidyEntities(input_rows):
  """Clean up entity names in rows."""
  return (map(TidyEntityName, row) for row in input_rows)

def HeuristicallyRelate(graph):
  """Assume that A influenced Ax and xA."""

  for node in graph:
    if len(node) < 2:  # prevent C -> Cobol
      continue

    node_lower = node.lower()
    for node2 in graph:
      if node == node2:  # Prevent self-links
        continue

      node2_lower = node2.lower()
      if node2_lower.startswith(node_lower) or node2_lower.endswith(node_lower):
        yield (node, node2)

def NormalizeParadigmSet(paradigms):
  result = set()
  for p in paradigms:
    pl = p.lower()
    if "functional" in pl or "declarative" in pl:
      result.add("functional")
    elif "procedural" in p.lower() or "imperative" in p.lower():
      result.add("imperative")
    elif "multi" in p.lower():
      result.add("multi")
    elif "object" in p.lower():
      result.add("object")
  return result


def ParadigmColorShape(paradigms):
  """Maps paradigms to colors and shape.

  Mixed: purple
  Imperative: maroon
  OO: navy / dashed
  Multi-paradigm: orange
  Functional: green / dotted
  Unknown: black
  """
  if not paradigms:
    return ('#000000', '')
  if len(paradigms) > 1:
    return ('#800080', 'house')  # Mixed
  p = iter(paradigms).next()
  return dict(
      imperative=('#800000', 'polygon'),
      functional=('#32cd32', 'diamond'),
      object=('#000080', 'octagon'),
      multi=('#ff8c00', ''),
      )[p]

def WriteInfluenceSubgraph(influence_bootstrap_list, graph, paradigms):
  log('Starting from %s', ", ".join(influence_bootstrap_list))
  nodes = reduce(set.union,
                 (graph.FindAllReachableUndirected(node)
                  for node in influence_bootstrap_list), set())
  log('Reachable nodes: %d', len(nodes))

  subgraph = graph.Subgraph(nodes)
  log('Restricted to %s', subgraph)
  subgraph.Extend(HeuristicallyRelate(subgraph))
  log('Extended to %s', subgraph)

  # Paradigm colors
  def NodeAttributes(node):
    normalized_set = NormalizeParadigmSet(paradigms.get(node, set()))
    color, shape = ParadigmColorShape(normalized_set)
    if not shape:
      shape = 'ellipse'
    return 'fontcolor="%s",color="%s",shape=%s' % (color, color, shape)

  node_attributes = ((node, NodeAttributes(node)) for node in subgraph.Nodes())

  # Printing data
  dot_data = '\n'.join(dot.PrintDigraph(subgraph, 'Influence', node_attributes))
  output_filename = '%s.png' % "-".join(influence_bootstrap_list)
  log('Starting graph vizualisation.')
  dot.Dotify(dot_data, output_filename,
      extra_args=['-Gratio=fill', '-Granksep=1.2', '-Nfontsize=24'])
  log('Genearted %s', output_filename)

def main(argv):
  # Read input
  input_rows = dbpedia_parse.ParseDbpediaCsv(open('data/infobox_en_influence.csv'))
  input_rows = TidyEntities(input_rows)

  input_rows, input_rows_copy = itertools.tee(input_rows)

  input_relations = dbpedia_parse.InterpretInfluence(input_rows)
  paradigms = dbpedia_parse.GetParadigms(input_rows_copy)

  # Graph processing
  graph = graphs.Graph(input_relations)
  log('Parsed %s', graph)

  if argv:
    WriteInfluenceSubgraph(argv, graph, paradigms)
  else:
    log("Looking for large connected subgraphs.")
    for connected_subgraph in graph.FindConnectedSubgraphs():
      if len(connected_subgraph.Nodes()) > 10:
        log('Found: %s. Sample node: %s',
            connected_subgraph, iter(connected_subgraph.Nodes()).next())
        nodes_by_order = sorted(connected_subgraph,
            key=lambda node: len(graph.Adjacent(node)), reverse=True)
        for node in nodes_by_order[:2]:
          influenced = connected_subgraph.Adjacent(node)
          print "%s influenced %d others: %s" % (node, len(influenced),
              ", ".join(influenced))
        print

    log("No arguments given.")
    log(__doc__)


if __name__ == '__main__':
  main(sys.argv[1:])
