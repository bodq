"""Generate GraphXML documents suitable for hypergraph.

See http://hypergraph.sf.net for details about GraphXML.

Sample usage:

  open('data.xml', 'w').write(
      graphxml.PrintDigraph(subgraph, 'Influence').toprettyxml())
"""

import xml.dom.minidom as dom

def PrintDigraph(graph, name):
  d = dom.getDOMImplementation()
  doctype = d.createDocumentType("GraphXML", "", "GraphXML.dtd")
  doc = d.createDocument("", "GraphXML", doctype)
  graph_node = doc.documentElement.appendChild(doc.createElement("graph"))
  graph_node.setAttribute("id", name)
  for node_name in graph:
    node = graph_node.appendChild(doc.createElement("node"))
    node.setAttribute("name", node_name)
    label_node = node.appendChild(doc.createElement("label"))
    label_node.appendChild(doc.createTextNode(node_name))

  for a, b in graph.iteritems():
    edge = graph_node.appendChild(doc.createElement("edge"))
    edge.setAttribute("source", a)
    edge.setAttribute("target", b)
    edge.setAttribute("isDirected", "true")

  return doc
