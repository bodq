"""Dot file format routines."""
import os
import subprocess

def PrintDigraph(graph, name, node_attributes=()):
  """Yields strings that reperesend the graph in .dot format."""
  yield 'digraph %s {' % name
  for node, attr_string in node_attributes:
    yield '"%s" [%s];' % (node, attr_string)
  for edge in graph.iteritems():
    yield '"%s" -> "%s";' % edge
  yield '}'

OSX_FONT = '/Library/Fonts/Verdana.ttf'

def Dotify(dot_data, output_filename, dot_command='dot', extra_args=[]):
  command = [dot_command, '-o', output_filename, '-Tpng'] + extra_args
  # HACK: On OSX, if gviz is installed with fink, the font must be specified.
  if os.path.exists(OSX_FONT):
    command.append('-Nfontname=' + OSX_FONT)
  child = subprocess.Popen(command, stdin=subprocess.PIPE)
  child.communicate(dot_data)
  if child.returncode != 0:
    raise RuntimeError("Could not execute %s" % command)
