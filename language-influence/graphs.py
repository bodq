"""Graph utils."""

class Graph(object):
  def __init__(self, relations):
    # Edges
    self.__forward = {}
    # Inverted edges
    self.__back = {}
    self.Extend(relations)

  def Extend(self, relations):
    for a, b in relations:
      self.__forward.setdefault(a, set()).add(b)
      self.__back.setdefault(b, set()).add(a)

  def Adjacent(self, start_node):
    return self.__forward.get(start_node, set())

  def AdjacentUndirected(self, start_node):
    return self.Adjacent(start_node).union(self.__back.get(start_node, set()))

  def FindAllReachableUndirected(self, start_node):
    visited = set()
    stack = [start_node]
    while stack:
      node = stack.pop()
      visited.add(node)
      for child in self.AdjacentUndirected(node):
        if child not in visited:
          stack.append(child)
    return visited

  def __iter__(self):
    return iter(self.Nodes())

  def iteritems(self):
    for node in self.__forward:
      for child in self.Adjacent(node):
        yield node, child

  def Nodes(self):
    return set(self.__forward).union(set(self.__back))

  def __str__(self):
    return "<Graph: %d nodes, %d edges>" % (
        len(self.Nodes()),
        len(list(self.iteritems())))

  def Subgraph(self, nodes):
    return Graph(
        (node, child)
        for node, children in self.__forward.iteritems()
        for child in children
        if node in nodes)

  def FindConnectedSubgraphs(self):
    unreached = self.Nodes()
    while unreached:
      first_unreachable = iter(unreached).next()
      component = self.FindAllReachableUndirected(first_unreachable)
      yield self.Subgraph(component)
      unreached = unreached.difference(component)
