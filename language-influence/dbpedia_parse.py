"""Parse dbpedia file format."""

import csv
import urllib

def ParseDbpediaCsv(input_file):
  r"""Yields 3-tuples with data from dbpedia csv file.

  Parses dbpedia CSV file describing infobox data. Input file format
  (tab-separated):
    <name> <property> <values> ...
  Output:
    (name, propertu, value)

  >>> import StringIO
  >>> i = lambda *rows: StringIO.StringIO(
  ...     '\n'.join('\t'.join(row) for row in rows) + '\n')
  >>> list(ParseDbpediaCsv(i([])))
  Traceback (most recent call last):
  ...
  AssertionError: Line [] should have at least 3 columns
  >>> list(ParseDbpediaCsv(i(['a', 'x', 'b', 'c'], ['d', 'z', 'e'])))
  [('a', 'b', 'x'), ('a', 'c', 'x'), ('d', 'e', 'z')]
  """
  result = {}

  for line in csv.reader(input_file, delimiter='\t'):
    assert len(line) > 2, 'Line %s should have at least 3 columns' % line
    line = map(urllib.unquote, line)
    name, property, values = line[0], line[1], line[2:]
    if values[-1] in ('r', 'l'):  # Dbpedia magic values?
      values.pop()
    for value in values:
      yield (name, value, property)


def InterpretInfluence(input_rows):
  """Interprets influence/influencedBy as forward/back link.

  Returns tuple (A, B) if A influenced by. Returns tuple (B, A) if B was
  influenced by A.

  >>> list(InterpretInfluence([('a', 'b', 'influence'),
  ...                          ('c', 'd', 'influencedBy')]))
  [('a', 'b'), ('d', 'c')]
  """
  ignored_relations = set()
  for source, destination, relation in input_rows:
    if relation == "influence":
      yield (source, destination)
    elif relation == "influencedBy":
      yield (destination, source)

def GetParadigms(input_rows):
  """Returns a dictionary mapping entity to set of paradigms."""
  result = {}
  for source, destination, relation in input_rows:
    if relation == "paradigm":
      result.setdefault(source, set()).add(destination)
  return result

