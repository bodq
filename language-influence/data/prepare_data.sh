#!/bin/bash
# Download infobox dataset from http://wiki.dbpedia.org/Downloads32

FILENAME=infobox_en.csv.bz2
RESULT=infobox_en_influence.csv
[ -e $RESULT.bz2 ] && { bunzip2 -k $RESULT.bz2; exit $?; }
[ -e "$FILENAME" ] || curl -O http://downloads.dbpedia.org/3.2/en/$FILENAME

bzcat $FILENAME | awk > $RESULT -F\t \
    '$2 == "influence" || $2 == "influencedBy" || $2 == "paradigm" {print}' 

echo "Done. Created $RESULT: $(echo $(wc -l < $RESULT)) lines."
